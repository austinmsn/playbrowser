(function () {
    var defaults;
    this.FileTake = function () {
        defaults = {
            contextPathUrl: '/PSPBase',
            url: 'hello',
            fileName: 'no-filename',
            contentType: 'application/json; charset=UTF-8',
            body: JSON.stringify({'data': 'test'}),
            blocking: true,
            isPrint: false,
            callAfter: function(obj){
            	console.log("exe callAfter");
            }
        }
        if (arguments[0]) {
            var property;
            for (property in arguments[0]) {
                if (defaults.hasOwnProperty(property)) {
                    defaults[property] = arguments[0][property];
                }
            }
        }

        /**
         * 用iframe 執行瀏覽器列印動作
         * 直接 call FileTake.click() 即可
         * 屬性需設定 isPrint 為ture
         * @param blob
         */
        FileTake.prototype.popPrintOnIframe = function (blob) {

            var fileURL = URL.createObjectURL(blob);

            $("#fileTake_pdfFrame").remove()
            $('<iframe id="fileTake_pdfFrame"></iframe>').appendTo("body").hide();
            $('#fileTake_pdfFrame').setAttribute('src', fileURL);
            document.getElementById('fileTake_pdfFrame').onload = function () {
                document.getElementById('fileTake_pdfFrame').focus();
                document.getElementById('fileTake_pdfFrame').contentWindow.print();
                window.URL.revokeObjectURL(fileURL);
            };

        }

        /**
         * 使用popBlob需注意bom編碼問題,參考下列:
         * var blob = new Blob(["\ufeff"+csvContent], {type : 'text/csv;charset=utf-8'});
         * new FileTake().popBlob(blob);
         * @param blob
         */
        FileTake.prototype.popBlob = function (blob) {
            var url = window.URL.createObjectURL(blob);
            var a = document.createElement('a');
            a.href = url;
            a.download = defaults.fileName;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        /**
         * 不需處理bom編碼問題,由method處理
         * @param csvContent
         */
        FileTake.prototype.popText = function (csvContent) {
            var encodedUri = encodeURI(csvContent);

            var link = document.createElement("a");
            link.setAttribute("href", "data:text/csv;charset=utf-8,%EF%BB%BF" + encodedUri);
            link.setAttribute("download", new Date().getTime() + ".csv");
            link.innerHTML = "Click Here to download";
            link.click();
        }

        //public method
        FileTake.prototype.click = function () {

            let popBlob = this.popBlob;
            if (defaults.isPrint) {
                popBlob = this.popPrintOnIframe;
            }

            if (defaults.blocking) {
                $('#waitingMsg').text("下載中，請稍後...");
                $('body').addClass("loading");
            }
            fetch(defaults.contextPathUrl + defaults.url, {
                credentials: 'include', method: 'POST',
                headers: {
                    'content-type': defaults.contentType
                },
                body: defaults.body,
            }).then(function (res) {
                switch (res.status) {
                    case 201:
                        //取得後端檔名 如果沒定義將使用後端給予的檔名
                        if(defaults.fileName == 'no-filename'){
                            var disposition = res.headers.get('content-disposition');
                            var fileName = disposition.replace(/attachment;.*filename=/, '').replace(/"/g, '');
                            defaults.fileName = fileName;
                        }
                        return res.blob();
                        break;
                    case 200:
                    case 417:
                    case 404:
                    case 500:
                        res.json().then(ok => {
                            var err = ok.msg;
                            console.log(err);
                            throw new Error(err);
                        }).catch(ng => alert(ng));
                        break;
                    case 504:
                        res.text().then(ok => {
                            var start = ok.indexOf("<b>Message</b>") + "<b>Message</b>".length;
                            var end = ok.indexOf("</p><p><b>Description</b>");
                            var err = ok.substring(start, end);
                            alert(err);
                        }).catch(ng => alert(ng));
                        break;
                    default:
                        res.text().then(ok => {
                            var start = ok.indexOf("<b>Message</b>") + "<b>Message</b>".length;
                            var end = ok.indexOf("</p><p><b>Description</b>");
                            var err = ok.substring(start, end);
                            console.log(err);
                            throw new Error(err);
                        }).catch(ng => alert(ng));
                        break;
                }
            }).then(function (data) {
                // 增加檢核undefined判斷,避免因無data導致js報錯
                if (typeof data !== 'undefined') {
                    popBlob(data);
                    defaults.callAfter(data);
                }
            }).finally((fin) => {
                if (defaults.blocking) $('body').removeClass("loading");
            })
        }
    }
})();